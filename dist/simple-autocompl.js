(function () {
    angular
        .module("simpleAutocompl", [])
        .directive("simpleAutocompl", directiveFactory);

    directiveFactory.$inject = ["$timeout", "$sce"];
    function directiveFactory($timeout, $sce) {
        //http://youmightnotneedjquery.com/#outer_height_with_margin
        function outerHeight(el) {
            var height = el.offsetHeight;
            var style = getComputedStyle(el);
            height += parseInt(style.marginTop) + parseInt(style.marginBottom);
            return height;
        }

        return {
            restrict: "AE",

            scope: {
                placeholder: "@",
                dataList: "<list",
                renderSuggestion: "<renderSuggFn",
                searchDelay: "<",
                minToSearch: "<",
                noSuggestionsText: "@",
                enableExpando: "<expando",
                enableClear: "<"
            },

            require: 'ngModel',

            template: "\
                <div class='border' ng-class='{clearable:enableClear,expando:enableExpando,updating:isUpdating,expanded:expanded}'>\
                    <input type='text' ng-disabled='disabled' ng-readonly='readonly' placeholder='{{placeholder}}' class='form-control'></span><button class='btn cleaner' type='button' ng-if='enableClear' ng-click='clear()'><span class='glyphicon glyphicon-remove'></span></button><button class='btn expander' type='button' ng-if='enableExpando' ng-click='toggleExpand()'><span class='glyphicon glyphicon-triangle-bottom'></span></button>\
                </div>\
                <ul ng-if='expanded' class='suggestions'>\
                    <li ng-if='isUpdating' class='update-indicator'>&nbsp;</li>\
                    <li ng-if='!isUpdating&&!suggestions.length' class='none'>{{noSuggestionsText}}</li>\
                    <li ng-repeat='suggestion in suggestions' ng-click='selectSuggestion(suggestion)' ng-mouseover='setCurrent(suggestion)' ng-class='{current:suggestion===currentSuggestion}' ng-bind-html='getTrustedHtml(suggestion)'></li>\
                </ul>",

            link: function (scope, element, attrs, ngModel) {
                var inputElement = element.find("input");
                var task = null;
                var stash = "";
                var doNotExpand = false;

                function trimValue(value) {
                    var trimmedValue = value.trim();
                    if (!trimmedValue && value) {
                        //если строка состояла из пробелов, считается, что пользователь желает найти один пробел
                        trimmedValue = " ";
                    }
                    return trimmedValue;
                }

                function textualize(htmlOrText) {
                    return htmlOrText instanceof HTMLElement ? htmlOrText.innerText.trim() : htmlOrText;
                }

                function getTestRegExp(str, global) {
                    return new RegExp(str.replace(/[\[\]\\/^$.|?*+(){}]/g, ""), "i" + (global ? "g" : ""));
                }

                //фильтрует список предложений, если он жёстко задан
                function filterSuggestions(suggestions, str) {
                    var filtered = [];
                    var testRegExp = getTestRegExp(str);
                    angular.forEach(suggestions, function (suggestion) {
                        var matches = false;
                        if (angular.isObject(suggestion)) {
                            for (var key in suggestion) {
                                if (key.indexOf("$$") == 0) continue;
                                if (testRegExp.test(suggestion[key].toString())) {
                                    matches = true;
                                    break;
                                }
                            }
                        }
                        else if (testRegExp.test(suggestion.toString())) {
                            matches = true;
                        }
                        if (matches) filtered.push(suggestion);
                    });
                    return filtered;
                }

                function cancelSearchTask() {
                    if (task) {
                        $timeout.cancel(task);
                        task = null;
                    }
                }

                function collapse() {
                    scope.expanded = false;
                }

                function expand() {
                    scope.expanded = true;
                };

                function collapseAndRestoreStash() {
                    inputElement.val(stash);
                    collapse();
                }

                //закрывает список, если он пуст, соответствующей подсказки не задано или не включён режим селекта и не взведён спецфлаг
                function collapseIfEmpty(dontCollapseForTextMode) {
                    if (!scope.suggestions.length && (!scope.noSuggestionsText || !scope.selectMode && !dontCollapseForTextMode)) {
                        scope.expanded = false;
                    }
                }

                var lastWaitingFor;
                function searchSuggestions(forceExpand) {
                    var str = trimValue(inputElement.val());
                    expand();
                    scope.currentSuggestion = null;
                    if (angular.isFunction(scope.dataList)) {
                        scope.suggestions = [];
                        var suggestionsRaw = scope.dataList(str);
                        if (angular.isFunction(suggestionsRaw.then)) {
                            scope.isUpdating = true;
                            lastWaitingFor = suggestionsRaw;
                            suggestionsRaw.then(function (suggestions) {
                                if (angular.isArray(suggestions)) {
                                    scope.suggestions = suggestions;
                                }
                                else if (angular.isArray(suggestions.data)) {
                                    scope.suggestions = suggestions.data;
                                }
                                collapseIfEmpty(forceExpand);
                            }).finally(function () {
                                if (lastWaitingFor == suggestionsRaw) {
                                    scope.isUpdating = false;
                                }
                            });
                        }
                        else if (angular.isArray(suggestionsRaw)) {
                            scope.suggestions = suggestionsRaw;
                        }
                    }
                    else {
                        scope.suggestions = filterSuggestions(scope.dataList, str);
                    }
                    collapseIfEmpty(forceExpand);
                }

                function findCurrentIndex() {
                    for (var i = 0; i < scope.suggestions.length; i++) {
                        if (scope.suggestions[i] === scope.currentSuggestion) return i;
                    }
                    return -1;
                }

                function setNoCurrent() {
                    scope.currentSuggestion = null;
                    inputElement.val(stash);
                }

                function setCurrentPrev() {
                    var currentIndex = findCurrentIndex();
                    if (currentIndex == 0) {
                        setNoCurrent();
                        return;
                    }
                    var movedDown;
                    var newCurrentIndex;
                    if (currentIndex == -1) {
                        newCurrentIndex = scope.suggestions.length - 1;
                        movedDown = true;
                    }
                    else {
                        newCurrentIndex = currentIndex - 1;
                        movedDown = false;
                    }
                    scope.setCurrent(scope.suggestions[newCurrentIndex]);
                    inputElement.val(textualize(renderSuggestionEnsured()(scope.currentSuggestion)));
                    return movedDown;
                }

                function setCurrentNext() {
                    var currentIndex = findCurrentIndex();
                    if (currentIndex == scope.suggestions.length - 1) {
                        setNoCurrent();
                        return;
                    }
                    var movedDown;
                    var newCurrentIndex;
                    if (currentIndex == -1) {
                        newCurrentIndex = 0;
                        movedDown = false;
                    }
                    else {
                        newCurrentIndex = currentIndex + 1;
                        movedDown = true;
                    }
                    scope.setCurrent(scope.suggestions[newCurrentIndex]);
                    inputElement.val(textualize(renderSuggestionEnsured()(scope.currentSuggestion)));
                    return movedDown;
                }

                function adjustScroll(movedDown) {
                    var suggestionsElement = element.find("ul");
                    var currentIndex = findCurrentIndex();
                    if (currentIndex < 0) return;
                    var currentSuggestionElement = suggestionsElement.children().eq(currentIndex);
                    var currentSuggestionElementTop = currentSuggestionElement[0].offsetTop;
                    if (movedDown) {
                        //вниз
                        var currentSuggestionElementHeight = outerHeight(currentSuggestionElement[0]);
                        if (currentSuggestionElementTop + currentSuggestionElementHeight > suggestionsElement[0].scrollTop + suggestionsElement[0].clientHeight) {
                            suggestionsElement[0].scrollTop = currentSuggestionElementTop + currentSuggestionElementHeight - suggestionsElement[0].clientHeight;
                        }
                    }
                    else {
                        //вверх
                        if (currentSuggestionElementTop < suggestionsElement[0].scrollTop) {
                            suggestionsElement[0].scrollTop = currentSuggestionElementTop;
                        }
                    }
                }

                function renderSuggestionDefault(suggestion) {
                    if (angular.isObject(suggestion)) {
                        var keys = Object.keys(suggestion);
                        if (keys.indexOf("name") > -1) return suggestion.name;
                        var rendered;
                        for (var key in suggestion) {
                            if (key.indexOf("$$") == 0) continue;
                            if (!rendered) {
                                rendered = suggestion[key];
                            }
                            else {
                                rendered += ", " + suggestion[key];
                            }
                        }
                        return rendered;
                    }
                    return suggestion.toString();
                }

                function renderSuggestionEnsured() {
                    return angular.isFunction(scope.renderSuggestion) ?
                        scope.renderSuggestion :
                        renderSuggestionDefault;
                };

                function highlightSuggestion(renderedSuggestion) {
                    var hlToken = trimValue(stash);
                    if (!hlToken) {
                        return renderedSuggestion instanceof HTMLElement ?
                            renderedSuggestion.outerHTML :
                            renderedSuggestion.toString();
                    }
                    var testRegExp = getTestRegExp(hlToken, true);
                    function highlightSring(str) {
                        return str.replace(testRegExp, "<span class='highlight'>$&</span>");
                    }
                    if (renderedSuggestion instanceof HTMLElement) {
                        var elems = renderedSuggestion.querySelectorAll("*");
                        for (var i = 0; i < elems.length; i++) {
                            var elem = elems[i];
                            if (!elem.querySelectorAll("*").length) {
                                //элемент без подэлементов
                                elem.innerHTML = highlightSring(elem.innerHTML);
                            }
                        }
                        return renderedSuggestion.outerHTML;
                    }
                    return highlightSring(renderedSuggestion.toString());
                };

                scope.selectMode = Object.keys(attrs).indexOf("selectMode") != -1;
                scope.expanded = false;
                scope.suggestions = [];
                scope.currentSuggestion;

                scope.selectSuggestion = function (suggestion) {
                    var textValue = textualize(renderSuggestionEnsured()(suggestion));
                    stash = textValue;
                    ngModel.$setViewValue(scope.selectMode ? suggestion : textValue);
                    inputElement.val(textValue);
                    collapse();
                    scope.suggestions = [];
                    doNotExpand = true;
                    inputElement[0].focus();
                };

                scope.setCurrent = function (suggestion) {
                    scope.currentSuggestion = suggestion;
                };

                scope.clear = function () {
                    scope.suggestions = [];
                    stash = "";
                    if (scope.expanded) {
                        //если при нажатии кнопки сброса был открыт список предложений,
                        //то метод collapseAndRestoreStash выставит значение поля ввода в
                        //stash, которое теперь содержит пустую строку
                        collapseAndRestoreStash();
                    }
                    else {
                        //иначе же нужно "вручную" опустошить поле ввода
                        inputElement.val("");
                    }
                    scope.suggestions = [];
                    ngModel.$setViewValue(scope.selectMode ? undefined : "");
                    doNotExpand = true;
                    inputElement[0].focus();
                };

                scope.getTrustedHtml = function (str) {
                    return $sce.trustAsHtml(highlightSuggestion(renderSuggestionEnsured()(str)));
                };

                scope.toggleExpand = function () {
                    if (scope.expanded) {
                        collapse();
                    }
                    else {
                        if (scope.suggestions.length) {
                            expand();
                        }
                        else {
                            cancelSearchTask();
                            searchSuggestions(true);
                        }
                        doNotExpand = true;
                        inputElement[0].focus();
                    }
                };

                inputElement.on("blur", function () {
                    cancelSearchTask();
                    if (!scope.expanded) return;
                    //задержка закрытия необходима, чтобы был возможен клик по строке предложения
                    $timeout(function () {
                        if (!scope.expanded) return;
                        //если blur вызвано выбором предложения, список уже закрыт в этот момент,
                        //иначе он открыт и его следует закрыть
                        collapseAndRestoreStash();
                    }, 200);
                });

                inputElement.on("blur", function () {
                    if (ngModel.$untouched) {
                        ngModel.$setTouched();
                        scope.$apply();
                    }
                });

                inputElement.on("focus", function () {
                    if (doNotExpand) {
                        //событие сработало после программного фокусирования элемента, не нужно открывать список
                        doNotExpand = false; //нужно сбросить признак, поскольку он одноразовый
                    }
                    else {
                        //событие сработало после фокусировки, вызванной взаимодействием с пользователем
                        if (scope.suggestions.length) {
                            scope.expanded = true;
                            scope.$apply();
                        }
                    }
                });

                inputElement.on("input", function (e) {
                    var inputValue = inputElement.val();
                    if (inputValue == stash) {
                        return;
                        //IE при изменениях в placeholder'е генерирует событие input и происходит паразитный поиск (и даже если "сознательного"
                        //изменения placeholder'а не происходит, то есть никто не меняет связанный с настоящей директивой атрибут, из-за ангулярных
                        //манипуляций на самом деле происходит изменение атрибута при компиляции директивы и срабатывает обработчик события input);
                        //данный блок позволяет обойти эту проблему
                    }
                    cancelSearchTask();
                    if (!scope.expanded) {
                        scope.suggestions = [];
                    }
                    stash = inputValue;
                    ngModel.$setViewValue(scope.selectMode ? undefined : inputValue);
                    if (scope.minToSearch && inputValue.length < scope.minToSearch) {
                        if (scope.expanded) {
                            collapse();
                            scope.$apply();
                        }
                    }
                    else {
                        task = $timeout(function () {
                            searchSuggestions();
                        }, isNaN(scope.searchDelay) || scope.searchDelay < 0 ? 0 : scope.searchDelay);
                    }
                });

                inputElement.on("keydown", function (e) {
                    if (scope.expanded) {
                        if (e.key == "Enter" && scope.currentSuggestion) {
                            e.preventDefault();
                            cancelSearchTask();
                            scope.selectSuggestion(scope.currentSuggestion);
                        }
                        else if (e.key == "Escape") {
                            cancelSearchTask();
                            collapseAndRestoreStash();
                        }
                        else if (e.key == "ArrowUp" || e.key == "ArrowDown") {
                            if (scope.isUpdating) return;
                            e.preventDefault();
                            switch (e.key) {
                                case "ArrowUp": adjustScroll(setCurrentPrev()); break;
                                case "ArrowDown": adjustScroll(setCurrentNext()); break;
                            };
                        }
                    }
                    else {
                        if (scope.enableExpando && e.key == "ArrowDown") {
                            if (scope.suggestions.length) {
                                expand();
                            }
                            else {
                                cancelSearchTask();
                                searchSuggestions();
                            }
                        }
                    }
                    scope.$apply();
                });

                ngModel.$render = function() {
                    if (ngModel.$viewValue === undefined || ngModel.$viewValue === null) {
                        stash = "";
                    }
                    else if (angular.isObject(ngModel.$viewValue) && scope.selectMode) {
                        stash = textualize(renderSuggestionEnsured()(ngModel.$viewValue));
                    }
                    else {
                        stash = ngModel.$viewValue.toString();
                    }
                    collapseAndRestoreStash();
                };

                attrs.$observe('disabled', function(value) {
                    scope.disabled = value;
                    if (value && scope.expanded) {
                        collapse();
                    }
                });

                attrs.$observe('readonly', function(value) {
                    scope.readonly = value;
                    if (value && scope.expanded) {
                        collapse();
                    }
                });
            }
        }
    }
})();
