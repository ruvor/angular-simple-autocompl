angular
    .module("testbed", ["simpleAutocompl"])
    .controller("ExampleController", ExampleController);

ExampleController.$inject = ["$timeout", "filterFilter"];
function ExampleController($timeout, filterFilter) {
    var task;
    this.retrievePlaces = function (str) {
        if (task) $timeout.cancel(task);
        return task = $timeout(function () {
            str = str.trim().toUpperCase();
            var places = filterFilter(self.places, function (value, index, array) {
                return value.country == self.country && value.name.toUpperCase().indexOf(str) >= 0;
            });
            return places;
        }, 1000);
    };

    this.renderPlace = function (place) {
        return place.name + " (" + place.country + ")"
    };

    this.renderObject = function (object) {
        var span = document.createElement("span");
        span.innerHTML = "<b>" + object.id + "</b> " + "<em>(" + object.name + ")</em>";
        return span;
    };

    this.getObjectJson = function (object) {
        return angular.toJson(object, 4);
    }

    var self = this;

    this.placeholders = {
        country: "Укажите страну..."
    };

    this.countries = [
        "Ангола",
        "Россия",
        "Гондурас",
        "СШП",
        { name: "Україна", type: "сказочная страна" }, //выберется name при показе
    ];

    this.places = [
        { name: "Кабинда", country: "Ангола" },
        { name: "Минусинск", country: "Россия" },
        { name: "Красноярск", country: "Россия" },
        { name: "Новосибирск", country: "Россия" },
        { name: "Омск", country: "Россия" },
        { name: "Лабытнанги", country: "Россия" },
        { name: "Тегусигальпа", country: "Гондурас" },
        { name: "Сан Педро Сула", country: "Гондурас" },
        { name: "Вашингтон", country: "СШП" },
        { name: "Гонолулу", country: "СШП" },
        { name: "Київ", country: "Україна" },
        { name: "Київ", country: "Днепр" },
    ];

    this.objects = [
        { id: 123, name: "Первый в своём роде" },
        { id: 234, name: "Второй в некотором роде" },
        { id: 345, name: "И третий для порядка" },
    ];

    this.names = [
        "Ўася",
        "Петя",
        "Ахмед",
        "Цзинь",
        "Гваделупе"
    ];
}
